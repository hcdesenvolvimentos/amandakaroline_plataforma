<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amandakaroline_plataforma
 */
global $configuracao;
?>

	<!-- RODAPE -->
		<footer class="rodape">
			<div class="container">
				<div class="bordaSuperior"></div>
				<div class="row">
					<div class="col-sm-3">
						<a href="<?php echo get_home_url(); ?>">
							<figure class="logoAk">
								<img src="<?php echo $configuracao['footer_logo']['url']; ?>" alt="Logo ak">
							</figure>
						</a>
					</div>		
					<div class="col-sm-6">
						<p class="copyright"><?php echo $configuracao['footer_copyright']; ?></p>
					</div>
					<div class="col-sm-3">
						<ul class="socialNetworks">
							<li><a href="<?php echo $configuracao['gerais_instagram']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/instagram.png" alt="Instagram"></a></li>
							<li><a href="<?php echo $configuracao['gerais_facebook']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/facebook.png" alt="Facebook"></a></li>
							<li><a href="<?php echo $configuracao['gerais_pinterest']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/pinterest.png" alt="Pinterest"></a></li>
							<li><a href="<?php echo $configuracao['gerais_loja']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/lojarodape.png" alt="AK Black Store"></a></li>
							<li><a href="<?php echo $configuracao['gerais_portfolio']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/portfolio.png" alt="AK Portfolio"></a></li>
							<li><a href="<?php echo $configuracao['gerais_blog']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/img/blog.png" alt="AK Blog"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
	</body>
	<!-- <div class="popupStore" style="display: none;">
		<span class="tooltipStore">Preços acessíveis, roupas novas e usadas. Modelos de alta qualidade!</span>
	</div> -->
</html>


<?php wp_footer(); ?>

</body>
</html>
