
<?php
/**
* Template Name: Contador
* Description: Contador
*
* @package amandakaroline_plataforma
*/

 ?>
 <head>
   <!-- Hotjar Tracking Code for https://amandakaroline.com.br/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1102349,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121354164-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121354164-1');
</script>

 </head>
 <title>Em breve Plataforma AK</title>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 <link href="https://fonts.googleapis.com/css?family=K2D:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="../js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
	<div class="pgContador">

<article class="hidden">
			<p>Amanda Karoline Olá, meu nome é Amanda Karoline, tenho 19 anos. Atualmente moro em Guarapuava. Modelo profissional.</p>
		</article>
		<div class="conteudoGeral" id="conteudoGeral">
			<div class="textosInicial">
				<h3 class="textos">Projeto AK</h3>
			</div>
			<div class="contador" id="conteudoGeral">
				<h3 id="dias" class="numeros">0</h3>
				<h3 id="horas" class="numeros">0</h3>
				<h3 id="minutos" class="numeros">0</h3>
				<h3 id="segundos" class="numeros">0</h3>
				<h3 id="milisegundos" class="numeros">0</h3>
			</div>
			<div class="redesSociais">
				<a target="_blank" href="https://www.facebook.com/AmandaKarolineOficial/?ref=br_rs"><i class="fab fa-facebook-f"></i></a>
				<a target="_blank" href="https://www.instagram.com/amandakarolineofc/?hl=pt-br"><i class="fab fa-instagram"></i></a>
			</div>
		</div>
</div>
<script>
// inicializando com a data
var contagemRegressiva = new Date("Nov 24, 2018 21:00:00").getTime();

	//ativando a função a cada um segundo
	var x = setInterval(function() {

	// pegando o horário atual
	var hoje = new Date().getTime();

	// subtraindo o tempo que resta entre a data escolhida e hoje
	var tempoRestante = contagemRegressiva - hoje;

	// contas pegas na internet para conseguir calcular os dias, horas, minutos e segundos
	var dias = Math.floor(tempoRestante / (1000 * 60 * 60 * 24));
	var horas = Math.floor((tempoRestante % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	var minutos = Math.floor((tempoRestante % (1000 * 60 * 60)) / (1000 * 60));
	var segundos = Math.floor((tempoRestante % (1000 * 60)) / 1000);
	var miliSegundos = (((tempoRestante % (1000 * 60)) / 1000) - segundos).toFixed(2).replace(/[^\d]+/g,'').replace(0,'');

	 if (segundos != 0 && segundos > -1) {

           
                // colcando no html na id "contador"
                document.getElementById('dias').innerHTML = dias;
                document.getElementById('horas').innerHTML = horas;
                document.getElementById('minutos').innerHTML = minutos;
                document.getElementById('segundos').innerHTML = segundos;
                document.getElementById('milisegundos').innerHTML = miliSegundos;
               
            }else if (dias == 0 && horas == 0 && minutos == 0 && segundos == 0 && miliSegundos == 0) {
                // colcando no html na id "contador"
                document.getElementById('dias').innerHTML  = 0;
                document.getElementById('horas').innerHTML  = 0;
                document.getElementById('minutos').innerHTML  = 0;
                document.getElementById('segundos').innerHTML = 0;
                document.getElementById('milisegundos').innerHTML  = 0;
               
                
            
            }

}, 1);
</script>

<style>



/* BODY */
  .pgContador{
       width: 100%;
       height: 267%;
    background: url(../img/fundo5.jpg);
    background-size: cover!important;
    background-position: center!important;
    background-attachment: fixed;
    position: absolute;
    z-index: 99999999999999999999999999999;
  }
   .pgContador:after{
    content: "";
    width: 100%;
    height: 111vh;
    background: rgba(0, 0, 0, 0.6588235294117647);
    display: block;
    position: fixed;
    top: 0;
    left: 0;
  }
   .pgContador article{
    display: none;
    opacity: hidden;
    visibility: hidden;
  }
     .pgContador article p{
      display: none;
      opacity: hidden;
      visibility: hidden;
    }

/* TOPO */
  .topo{

  }
  header{

  }

/*RODAPÉ*/
  .rodape{

  }
  footer{

  }

/*COPYRIGHT*/
  .copyright{

  }

/*PG*/
  .pg{

  }

/*PÁGINA INICIAL*/
  .conteudoGeral{
    color: #fff;
    position: relative;
    z-index: 2;
    text-align: center;
    font-family: 'K2D', sans-serif;
  }
    .conteudoGeral .textosInicial{
      
    }
      .conteudoGeral .textosInicial h3{
        margin: 0;
        font-size: 80px;
        margin-top: 110px;
      }
    .conteudoGeral .contador{
      margin: 180px 0;
      position: relative;
      text-align: center;
    }
    .conteudoGeral .contador:before{
      content: "Contagem regressiva";
      font-family: 'K2D', sans-serif;
      font-size: 34px;
      position: absolute;
      top: 0;
      width: 100%;
      left: 0;
    }
      .conteudoGeral .contador h3{
        display: inline-block;
        margin-top: 70px;
        margin: 70px 30px 0;
        font-size: 49px;
        background: #000;
        padding: 37px;
        width: 100%;
        max-width: 150px;
        position: relative;
      }
      .conteudoGeral .contador h3:after{
        content: "Dias";
        display: block;
        width: 100%;
        max-width: 150px;
        text-transform: lowercase;
        font-size: 23px;
        position: absolute;
        bottom: -30px;
        left: 50%;
        transform: translateX(-50%);
      }
      .conteudoGeral .contador h3#dias:after{
        content: "dias";
      }
      .conteudoGeral .contador h3#horas:after{
        content: "horas";
      }
      .conteudoGeral .contador h3#minutos:after{
        content: "minutos";
      }
      .conteudoGeral .contador h3#segundos:after{
        content: "segundos";
      }
      .conteudoGeral .contador h3#milisegundos:after{
        content: "milésimos";
      }
      .conteudoGeral .redesSociais{
        position: relative;
        bottom: 0;
        left: 0;
        text-align: center;
        width: 100%;
      }
        .conteudoGeral .redesSociais a{
          display: inline-block;
          color: #fff;
          font-size: 70px;
          text-align: center;
          margin: 0 50px;
          text-decoration: none;
        }
          .conteudoGeral .redesSociais a i{
            
          }

/*EXTRAS*/
  body::-webkit-scrollbar{ background: rgba(0, 0, 0, 0.6588235294117647);width: 10px;}
  body::-webkit-scrollbar-button{ background: #000;}
  body::-webkit-scrollbar-track{}
  body::-webkit-scrollbar-track-piece {}
  body::-webkit-scrollbar-thumb{ background: #000;}
  body::-webkit-scrollbar-corner {}
  body::-webkit-resizer{}

/*@MEDIA QUERY*/
  @media(max-width: 1200px){

  }

  @media(max-width: 991px){
  
  }
  @media(max-width: 768px){
    .conteudoGeral .contador h3 {
      font-size: 24px;
      max-width: 100px;
    }
    .conteudoGeral .redesSociais a {
      display: inline-block;
      color: #fff;
      font-size: 40px;
      text-align: center;
      margin: 0 10px;
      text-decoration: none;
    }
    .conteudoGeral .contador h3:after{
      font-size: 20px;
    }
    .conteudoGeral .contador {
      margin: 90px 0;
    }
    .conteudoGeral .textosInicial h3 {
      font-size: 60px;
    }
    .conteudoGeral .contador:before {
      font-size: 28px;
    }
  }

  @media(max-width: 445px){
    .conteudoGeral .textosInicial h3 {
      font-size: 40px;
    }
    .conteudoGeral .contador:before {
      font-size: 21px;
      top: -40px;
    }
    .conteudoGeral .contador h3 {
      display: block;
      margin: 10px auto 40px;
    }
    .conteudoGeral .redesSociais a {
      font-size: 29px;
    }
  }

  @media(max-width: 320px){
  
  }

</style>