
<?php
/**
* Template Name: Inicial
* Description: Página Inicial
*
* @package amandakaroline_plataforma
*/
get_header();
 ?>
		<!-- PAGINA INICIAL -->
		<div class="pg pg-inicial">
			<!-- SESSÃO INICIAL / BEM VINDO -->
			<section class="welcomePlataform">

				<h6><?php echo $configuracao['inicial_titulo']; ?></h6>
			</section>

			<!-- SESSÃO SOBRE A PLATAFORMA -->
			<section class="aboutPlataform" id="descricaoPlataforma">
				<div class="container">
					<h6><?php echo $configuracao['descricao_plataforma_titulo'] ?></h6>
					<div class="row">
						<div class="col-md-6">
							<figure>
								<img src="<?php echo $configuracao['descricao_plataforma_imagem']['url'] ?>" alt="Imagem">
							</figure>
						</div>	
						<div class="col-md-6">
							<div class="textContent">
								<?php echo $configuracao['descricao_plataforma_conteudo']; ?>
							</div>
						</div>	
					</div>
				</div>
			</section>

			<!-- SESSÃO DOS HEXAGONOS -->
			<section class="mosaicLayers">
				<h6 class="hidden">Links plataforma</h6>
				<div class="container">
					<div class="hexagonsLists">
						<div class="row">
							<div class="col-xs-12">
								<div class="hexagon hexagonUp" id="projetos">
								<?php 
									// LOOP PLATAFORMA PORTFOLIO
									$postPlataforma = new WP_Query(array(
										'post_type'     => 'plataformas',
										'posts_per_page'   => 1,
										'tax_query'     => array(
											array(
												'taxonomy' => 'categoriaPlataformas',
												'field'    => 'slug',
												'terms'    => 'portfolio',
											)
										)
									)
									);
									if ( $postPlataforma->have_posts() ) : $postPlataforma->the_post();
										$imagemPlataforma = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$imagemPlataforma = $imagemPlataforma[0];
										$linkPlataforma = rwmb_meta('Plataforma_link_plataforma');
										$galeria_imagens_plataforma = rwmb_meta('Plataforma_galeria_plataforma');
								?>
									<small title="Clique para ver mais detalhes" class="disponivel hvr-hang title"><img src="<?php echo $imagemPlataforma; ?>" alt="<?php echo get_the_title(); ?>" 
										data-descricaoPlataforma="<?php echo the_content(); ?>" 
										data-linkPlataforma="<?php echo $linkPlataforma ?>" 
										data-nomePlataforma="<?php echo get_the_title(); ?>"
										data-galeria-plataforma="<?php 
										foreach ($galeria_imagens_plataforma as $galeria_imagens_plataforma ){ 
											$galeria_imagens_plataforma = $galeria_imagens_plataforma['full_url'];
											echo $galeria_imagens_plataforma." | "; } ?>"
										>
										<span><?php echo get_the_title(); ?></span>
									</small>
								</div>
							</div>
						</div>
						<?php endif; wp_reset_query(); ?>
						<div class="row">
							<div class="col-xs-6">
								<div class="hexagon hexagonLeft">
									<?php 
									// LOOP PLATAFORMA BLOG 1
									$postPlataforma = new WP_Query(array(
										'post_type'     => 'plataformas',
										'posts_per_page'   => 1,
										'tax_query'     => array(
											array(
												'taxonomy' => 'categoriaPlataformas',
												'field'    => 'slug',
												'terms'    => 'blog1',
											)
										)
									)
									);
									if ( $postPlataforma->have_posts() ) : $postPlataforma->the_post();
										$imagemPlataforma = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$imagemPlataforma = $imagemPlataforma[0];
										$linkPlataforma = rwmb_meta('Plataforma_link_plataforma');
										$galeria_imagens_plataforma = rwmb_meta('Plataforma_galeria_plataforma');
									?>
									<small title="Clique para ver mais detalhes" class="disponivel hvr-hang title"><img src="<?php echo $imagemPlataforma; ?>" 
										data-descricaoPlataforma="<?php echo the_content(); ?>" alt="<?php echo get_the_title(); ?>" 
										data-linkPlataforma="<?php echo $linkPlataforma ?>" 
										data-nomePlataforma="<?php echo get_the_title(); ?>"
										data-galeria-plataforma="<?php 
											foreach ($galeria_imagens_plataforma as $galeria_imagens_plataforma ){ 
												$galeria_imagens_plataforma = $galeria_imagens_plataforma['full_url'];
												echo $galeria_imagens_plataforma." | "; } ?>"
										><span><?php echo get_the_title(); ?></span>
									</small>
								<?php endif; wp_reset_query(); ?>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="hexagon hexagonRight">
									<?php 
									// LOOP PLATAFORMA BLOG 2
									$postPlataforma = new WP_Query(array(
										'post_type'     => 'plataformas',
										'posts_per_page'   => 1,
										'tax_query'     => array(
											array(
												'taxonomy' => 'categoriaPlataformas',
												'field'    => 'slug',
												'terms'    => 'blog2',
											)
										)
									)
									);
									if ( $postPlataforma->have_posts() ) : $postPlataforma->the_post();
										$imagemPlataforma = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$imagemPlataforma = $imagemPlataforma[0];
										$linkPlataforma = rwmb_meta('Plataforma_link_plataforma');
										$galeria_imagens_plataforma = rwmb_meta('Plataforma_galeria_plataforma');
									?>
									<small class="indisponivel">
										<img src="<?php echo $imagemPlataforma; ?>" 
										data-descricaoPlataforma="<?php echo the_content(); ?>" 
										alt="<?php echo get_the_title(); ?>" 
										data-linkPlataforma="<?php echo $linkPlataforma ?>" 
										data-nomePlataforma="<?php echo get_the_title(); ?>"
										data-galeria-plataforma="<?php 
										foreach ($galeria_imagens_plataforma as $galeria_imagens_plataforma ){ 
											$galeria_imagens_plataforma = $galeria_imagens_plataforma['full_url'];
											echo $galeria_imagens_plataforma." | "; } ?>"><span><?php echo get_the_title(); ?></span>
									</small>
									<?php endif; wp_reset_query(); ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="hexagon hexagonDown">
									<?php 
									// LOOP PLATAFORMA BLOG 2
									$postPlataforma = new WP_Query(array(
										'post_type'     => 'plataformas',
										'posts_per_page'   => 1,
										'tax_query'     => array(
											array(
												'taxonomy' => 'categoriaPlataformas',
												'field'    => 'slug',
												'terms'    => 'loja',
											)
										)
									)
									);
									if ( $postPlataforma->have_posts() ) : $postPlataforma->the_post();
										$imagemPlataforma = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$imagemPlataforma = $imagemPlataforma[0];
										$linkPlataforma = rwmb_meta('Plataforma_link_plataforma');
										$galeria_imagens_plataforma = rwmb_meta('Plataforma_galeria_plataforma');
									?>
									<small class="linkLoja indisponivel">
										<img  src="<?php echo $imagemPlataforma; ?>" 
										data-descricaoPlataforma="<?php echo the_content(); ?>"
										alt="<?php echo get_the_title(); ?>" 
										data-linkPlataforma="<?php echo $linkPlataforma ?>" 
										data-nomePlataforma="<?php echo get_the_title(); ?>"
										data-galeria-plataforma="<?php 
										foreach ($galeria_imagens_plataforma as $galeria_imagens_plataforma ){ 
											$galeria_imagens_plataforma = $galeria_imagens_plataforma['full_url'];
											echo $galeria_imagens_plataforma." | "; } ?>"><span><?php echo get_the_title(); ?></span>
									</small>
									<?php endif; wp_reset_query(); ?>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</section>

			<!-- SESSÃO INFORMAÇÕES DO PROJETO -->
			<section class="projectInfo" data-informacao="<?php echo $configuracao['resumo_descricao']; ?>"> <!-- ESSA SESSÃO É UM SKEW -->
				<div class="antiSkew">	<!-- ESSA DIV DESFAZ O SKEW PARA O CONTEUDO FICAR RETO HORIZONTALMENTE -->
					<h6 id="diferenciais">Projeto:</h6>
					<div class="advantagesList"> <!-- LISTA DE VANTAGENS DO PROJETO -->
						<ul>
							<li>
								<figure>
									<img src="<?php echo $configuracao['vantagem_1_imagem']['url']; ?>" alt="Image">
								</figure>
								<h3><?php echo $configuracao['vantagem_1_titulo']; ?></h3>
								<p><?php echo $configuracao['vantagem_1_descricao']; ?></p>
							</li>
							<li>
								<figure>
									<img src="<?php echo $configuracao['vantagem_2_imagem']['url']; ?>" alt="Image">
								</figure>
								<h3><?php echo $configuracao['vantagem_2_titulo']; ?></h3>
								<p><?php echo $configuracao['vantagem_2_descricao']; ?></p>
							</li>
							<li>
								<figure>
									<img src="<?php echo $configuracao['vantagem_3_imagem']['url']; ?>" alt="Image">
								</figure>
								<h3><?php echo $configuracao['vantagem_3_titulo']; ?></h3>
								<p><?php echo $configuracao['vantagem_3_descricao']; ?></p>
							</li>
							<li>
								<figure>
									<img src="<?php echo $configuracao['vantagem_4_imagem']['url']; ?>" alt="Image">
								</figure>
								<h3><?php echo $configuracao['vantagem_4_titulo']; ?></h3>
								<p><?php echo $configuracao['vantagem_4_descricao']; ?></p>
							</li>
						</ul>
					</div>

					<div class="container"> <!-- CONTAINER BOOTSTRAP -->
						<!-- IDEALIZADORA DO PROJETO -->
						<div class="projectIdealizer">
							<h2 id="idealizadora">Idealizadora do projeto:</h2> <!-- TITULO DA SESSÃO -->

							<!-- INFORMAÇÕES DA IDEALIZADORA -->
							<article class="idealizerInfo"> 
								<figure>
									<img src="<?php echo $configuracao['idealizadora_imagem']['url'];?>" alt="Imagem Idealizadora">
								</figure>
								<h3 class="idealizerName"><?php echo $configuracao['idealizadora_nome'];?></h3>
							</article>
						</div>

						<!-- MODELOS PARTICIPANTES -->
						<div class="participatingModels">
							<h4>Modelos participantes</h4>
							<!-- PRIMEIRO CARROSSEL DAS MODELOS PARTICIPANTES -->
							<div class="carouselModels" id="carouselModelsUp">
								<?php 
								// LOOP MODELOS PARTICIPANTES
								$postModelosParticipantesCima = new WP_Query(array(
										'post_type'     => 'modelos',
										'posts_per_page'   => -1,
										'tax_query'     => array(
											array(
												'taxonomy' => 'categoriaModelos',
												'field'    => 'slug',
												'terms'    => 'carrosselCima',
											)
										)
									)
								);
								while ( $postModelosParticipantesCima->have_posts() ) : $postModelosParticipantesCima->the_post();
									$imagemModeloCima = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$imagemModeloCima = $imagemModeloCima[0];
									$contatoPlataforma = rwmb_meta('Plataforma_link_contatoPlataforma');
										$galeria_imagens_modelo = rwmb_meta('Plataforma_galeria_modelo');
										$descricaoModelo = rwmb_meta('Plataforma_descricao_modelo');
										
										
							 ?>
								<!-- ITEM -->
								<div class="item <?php if($galeria_imagens_modelo){ echo  "disponivel";}else{echo " ";} ?>" title="Clique para ver mais detalhes da modelo" class="disponivel hvr-hang title"><img 
										data-nomePlataforma="<?php echo get_the_title(); ?>"
										data-descricaoModelo="<?php echo the_content(); ?>" 
										data-contatoPlataforma="<?php echo $contatoPlataforma ?>" data-galeria-modelo="<?php 
										foreach ($galeria_imagens_modelo as $galeria_imagens_modelo ){ 
											$galeria_imagens_modelo = $galeria_imagens_modelo['full_url'];
											echo $galeria_imagens_modelo." | "; } ?>"
										>
									<figure>
										<img src="<?php echo $imagemModeloCima; ?>" alt="Imagem Modelo">
									</figure>
									<h3><?php echo get_the_title(); ?></h3>
								</div>
							<?php endwhile; wp_reset_query();?>
							</div>

							<!-- SEGUNDO CARROSSEL DAS MODELOS PARTICIPANTES -->
							<div class="carouselModels" id="carouselModelsDown">
								<?php 
								// LOOP MODELOS PARTICIPANTES
								$postModelosParticipantesBaixo = new WP_Query(array(
										'post_type'     => 'modelos',
										'posts_per_page'   => -1,
										'tax_query'     => array(
											array(
												'taxonomy' => 'categoriaModelos',
												'field'    => 'slug',
												'terms'    => 'carrosselBaixo',
											)
										)
									)
								);
								while ( $postModelosParticipantesBaixo->have_posts() ) : $postModelosParticipantesBaixo->the_post();
									$imagemModeloBaixo = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$imagemModeloBaixo = $imagemModeloBaixo[0];
									$contatoPlataforma = rwmb_meta('Plataforma_link_contatoPlataforma');
										$galeria_imagens_modelo = rwmb_meta('Plataforma_galeria_modelo');
										$descricaoModelo = rwmb_meta('Plataforma_descricao_modelo');
										
										if($galeria_imagens_modelo ){
											$disponibilidade = "disponivel";
										}
								?>
								<!-- ITEM -->
								<div class="item <?php if($galeria_imagens_modelo){ echo  "disponivel";}else{echo " ";} ?>" title="Clique para ver mais detalhes da modelo" class="disponivel hvr-hang title"><img 
										data-nomePlataforma="<?php echo get_the_title(); ?>"
										data-descricaoModelo="<?php echo the_content(); ?>" 
										data-contatoPlataforma="<?php echo $contatoPlataforma ?>" data-galeria-modelo="<?php 
										foreach ($galeria_imagens_modelo as $galeria_imagens_modelo ){ 
											$galeria_imagens_modelo = $galeria_imagens_modelo['full_url'];
											echo $galeria_imagens_modelo." | "; } ?>"
										>
									<figure>
										<img src="<?php echo $imagemModeloBaixo; ?>" alt="Imagem Modelo">
									</figure>
									<h3><?php echo get_the_title(); ?></h3>
								</div>
								<?php endwhile; wp_reset_query();?>
							</div>

							
						</div>
					</div>
				</div>
			</section>

			<!-- SESSÃO DE PARCEIROS -->
			<section class="partners">
				<h6>Parceiros</h6>
				<div class="container">
					<!-- CARROSSEL DE PARCEIROS -->
					<div class="partnersCarousel" id="partnersCarousel">
						<?php 
							// LOOP MODELOS PARTICIPANTES
							$postParceiro = new WP_Query(array(
									'post_type'     => 'parceiros',
									'posts_per_page'   => -1,
								)
							);
							while ( $postParceiro->have_posts() ) : $postParceiro->the_post();
									$imagemParceiro = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$imagemParceiro = $imagemParceiro[0];
									$linkPartner = rwmb_meta('Plataforma_link_parceiro');
							?>
					 	?>
						<!-- ITEM -->
						<div class="item">
							<a href="<?php echo $linkPartner; ?>" target="_blank" class="linkPartner">
								<figure class="partnerLogo">
									<img src="<?php echo $imagemParceiro; ?>" alt="<?php echo get_the_title(); ?>">
								</figure>
							</a>
						</div>
					<?php endwhile; wp_reset_query(); ?>
					</div>
				</div>
			</section>
		</div>

		<!-- PAGINA MODAL -->
		<div class="pg pg-modal">
			<div class="row">
				<div class="col-sm-6">
					<!-- INFORMAÇÕES DA PAGINA -->
					<section class="infoPagina">
						<span class="fecharModal"><i class="far fa-times-circle"></i></span>
						<h6 class="tituloPagina"></h6>
						<div class="conteudo">
							
						</div>
						<a href="" target="_blank" class="linkModal" id="link_plataforma">Ir ao site</a>
					</section>
				</div>
				<div class="col-sm-6">
					<div class="imagensLink">
						<ul>
						</ul>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>
