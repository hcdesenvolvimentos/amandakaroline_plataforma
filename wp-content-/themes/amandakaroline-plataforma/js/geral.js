$(function(){
    $(document).ready(function(){
            setTimeout(function(){ 
                $(".loading").addClass("green");
                $("body").addClass('travarScroll');
                $(".loading .centerAll span.aguarde").addClass('fadeOut');
                $("body").addClass('travarScroll');
                $(".loading .centerAll span.carregandoSite").removeClass('fadeOut');
            }, 2000);
            
        })

        $(window).load(function() {

            setTimeout(function(){ 
                $(".loading .centerAll span.carregandoSite").addClass('fadeOut');
                $("body").addClass('travarScroll');
                $(".loading .centerAll span.wellcome").removeClass('fadeOut');
                $("body").addClass('travarScroll');
                $(".loading").removeClass("green");
                $(".loading").addClass("pink");
            }, 1000);
            setTimeout(function(){ 
                $(".loading").fadeOut();
                $("body").removeClass('travarScroll');


            }, 2000);
        })

       $(document).ready(function(){
            setTimeout(function(){ 
                $(".loading").addClass("green");
                $(".loading .centerAll span.aguarde").addClass('fadeOut');
                $("body").addClass('travarScroll');
                $(".loading .centerAll span.carregandoSite").removeClass('fadeOut');
            }, 2000);
            
        })

        $(window).load(function() {

            setTimeout(function(){ 
                $(".loading .centerAll span.carregandoSite").addClass('fadeOut');
                $("body").addClass('travarScroll');
                $(".loading .centerAll span.wellcome").removeClass('fadeOut');
                $("body").addClass('travarScroll');
                $(".loading").removeClass("green");
                $(".loading").addClass("pink");
            }, 1000);
            setTimeout(function(){ 
                $(".loading").fadeOut();
                $("body").removeClass('travarScroll');


            }, 2000);
        })

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	//CARROSSEL DE MODELOS PARTICIPANTES CIMA
	$("#carouselModelsUp").owlCarousel({
		items : 4,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
            768:{
            	items:3
            },
           
            991:{
                items:3
            },
            1100:{
                items:4
            },
            1440:{
                items:4
            },
            			            
        }		    		   		    
	    
	});

	//CARROSSEL DE MODELOS PARTICIPANTES CIMA
	$("#carouselModelsDown").owlCarousel({
		items : 4,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
            768:{
            	items:3
            },
           
            991:{
                items:3
            },
            1100:{
                items:4
            },
            1440:{
                items:4
            },
            			            
        }		    		   		    
	    
	});
	//CARROSSEL DE PARCEIROS
	$("#partnersCarousel").owlCarousel({
		items : 4,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
            768:{
            	items:3
            },
           
            991:{
                items:3
            },
            1100:{
                items:4
            },
            1440:{
                items:4
            },
            			            
        }		    		   		    
	    
	});
    $(".pg-inicial section.mosaicLayers .hexagonsLists .hexagon small.disponivel").click(function(e) {
        $(".pg-modal").addClass("abrirModal");
        $("body").addClass("travarScroll");
        let nomePlataforma = $(this).children('img').attr("data-nomePlataforma");
        let descricaoPlataforma = $(this).children('img').attr("data-descricaoPlataforma");
        let galeriaPlataforma = $(this).children('img').attr("data-galeria-plataforma").split(" | ");
        let linkPlataforma = $(this).children('img').attr("data-linkPlataforma");



        for (var i = 0 ; i < galeriaPlataforma.length - 1; i++) {
                $('.pg-modal .imagensLink ul').append("<li class='imagensGaleria'><img src='"+galeriaPlataforma[i]+"'></li>"); 
        }

        $(".pg-modal .infoPagina h6.tituloPagina").text(nomePlataforma);
        $(".pg-modal .infoPagina .conteudo").append(descricaoPlataforma);
        $(".pg-modal .infoPagina a.linkModal").attr("href",linkPlataforma);
    });

    // SCRIPT PARA DETECTAR O 'esc' DO TECLADO PARA FECHAR O MODAL DOS DETALHES DO PORTFOLIO
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $(".pg-modal").removeClass('abrirModal');
            $("body").removeClass("travarScroll");
            $('.pg-modal .imagensLink ul .imagensGaleria').remove();
            $(".pg-modal .infoPagina .conteudo p").remove();
            $(".pg-modal .infoPagina a.linkModal").attr("href","");
        }
    });

    $(".pg-modal .infoPagina span.fecharModal").click(function(e){
        $(".pg-modal").removeClass("abrirModal");
        $("body").removeClass("travarScroll");
        $('.pg-modal .imagensLink ul .imagensGaleria').remove();
        $(".pg-modal .infoPagina .conteudo p").remove();
        $(".pg-modal .infoPagina a.linkModal").attr("href","");
    });



   // SCRIPT PARA ABRIR A MODAL DAS MODELOS 
    $(".pg-inicial section.projectInfo .antiSkew .participatingModels .carouselModels .item.disponivel").click(function(e) {
        $(".pg-modal").addClass("abrirModalModelos");
        $(".pg-modal ").addClass("styleModel");

        $("body").addClass("travarScroll");
        let nomePlataforma = $(this).children('img').attr("data-nomePlataforma");
        let descricaoModelo = $(this).children('img').attr("data-descricaoModelo");
        let linkPlataforma = $(this).children('img').attr("data-linkPlataforma");
        let galeriaModelo = $(this).children('img').attr("data-galeria-modelo").split(" | ");



       for (var i = 0 ; i < galeriaModelo.length - 1; i++) {
                $('.pg-modal .imagensLink ul').append("<li class='imagensGaleria'><img src='"+galeriaModelo[i]+"'></li>"); 
        }

        $(".pg-modal .infoPagina h6.tituloPagina").text(nomePlataforma);
        $(".pg-modal .infoPagina .conteudo").append(descricaoModelo);
        $(".pg-modal .infoPagina a.linkModal").attr("href",linkPlataforma);

        $(document).ready(function() {
        $('#link_plataforma').html("Entrar em contato");
        });
       
               
    });

    $(".pg-modal .infoPagina span.fecharModal").click(function(e){
        $(".pg-modal").removeClass("abrirModalModelos");
        $(".pg-modal ").removeClass("styleModel");

        $("body").removeClass("travarScroll");
        $('.pg-modal .imagensLink ul .imagensGaleria').remove();
        $(".pg-modal .infoPagina .conteudo p").remove();
        $(".pg-modal .infoPagina a.linkModal").attr("href","");

        $(document).ready(function() {
        $('#link_plataforma').html("Ir ao site");
        });

    });
 
    $(document).ready(function() {
        $(".topo .navbar .navbar-header nav.navbar-collapse ul li a").addClass('hvr-underline-from-center');
        //hvr-underline-from-center  //hvr-underline-from-left
    });

    $('.anchor a').click(function() {
        $(".navbar-collapse").removeClass('in');
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 10
                }, 1200);
                return false;
            }
        }
        
    });
});