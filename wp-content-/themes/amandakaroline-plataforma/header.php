<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amandakaroline_plataforma
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">



	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" /> 

		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121354164-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121354164-1');
</script>

<!-- Hotjar Tracking Code for https://amandakaroline.com.br/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1102349,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php if (get_page_link() == "https://amandakaroline.com.br/contato/" ):?>

	<!-- TOPO -->
	<header class="topo contato">
		<div class="row">
			<!-- LOGO -->
			<div class="col-sm-2">
				<a href="<?php echo get_home_url(); ?>" class="logoPlataforma">
					<img class="" src="<?php echo $configuracao['header_logo']['url'] ?>" alt="Logo Amanda Karoline">
				</a>
			</div>
			<!-- MENU  -->	
			<div class="col-sm-10">
				<div class="navbar" role="navigation">	
					<!-- MENU MOBILE TRIGGER -->
					<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!--  MENU MOBILE-->
					<div class="row navbar-header">			
						<nav class="collapse navbar-collapse" id="collapse">
								<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Contato',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'nav navbar-nav',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>
						</nav>						
					</div>			
				</div>
			</div>
		</div>
	</header>
<?php else: ?>


		<!-- TOPO -->
	<header class="topo pg-inicial">
		<div class="row">
			<!-- LOGO -->
			<div class="col-sm-2">
				<a href="<?php echo get_home_url(); ?>" class="logoPlataforma">
					<img class="" src="<?php echo $configuracao['header_logo']['url'] ?>" alt="Logo Amanda Karoline">
				</a>
			</div>


			<!-- MENU  -->	
			<div class="col-sm-10">
				<div class="navbar" role="navigation">	
					<!-- MENU MOBILE TRIGGER -->
					<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!--  MENU MOBILE-->
					<div class="row navbar-header">			
						<nav class="collapse navbar-collapse" id="collapse">
								<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Principal',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'nav navbar-nav',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>
						</nav>						
					</div>			
				</div>
			</div>
		</div>
	</header>
<?php endif; ?>

