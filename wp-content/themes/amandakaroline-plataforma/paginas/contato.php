<?php
/**
 * Template Name: Contato
 * Description: Contato
 *
 * @package AmandaKaroline_plataforma
 */
	$fotoContato = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoContato = $fotoContato[0];
    get_header(); ?>
		<!-- PAGINA CONTATO -->
		<div class="pg pg-contato">
		
			<!-- SESSÃO CONTATO A PLATAFORMA -->
			<section class="contactPlataform">
				<div class="container">

				    <!-- SESSÃO FOTO -->
					<div class="imagemContato">
						<figure>
							<img src="<?php echo $fotoContato ?>" alt="Amanda Karoline">
						</figure>
					</div>		
					<form action="" method="post">
						<?php echo do_shortcode('[contact-form-7 id="5" title="Formulário de contato"]'); ?>
						<!-- <div class="row">
							<h6>Entre em Contato</h6>
							<div class="col-md-4">
							    <label for="nome"> Seu Nome</label>
							    <input type="text" id="nome" />

						        <label for="email">Seu e-email:</label>
						        <input type="text" id="email" />
						    </div>
							
						    <div class="col-md-8">
							     <label for="">Mensagem:</label>
							     <textarea></textarea> 

							     <input type="submit"> </input>
							</div>
						</div> -->
					</form>
				</div>
			</section>

		</div>
<?php get_footer(); ?>