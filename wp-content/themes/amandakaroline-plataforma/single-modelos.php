<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package amandakaroline_plataforma
 */

get_header();
?>
<div class="pg pg-modal single">
	<div class="row">
		<div class="col-sm-6">
			<!-- INFORMAÇÕES DA PAGINA -->
			<section class="infoPagina">
				<span class="fecharModal"><i class="far  fa-arrow-alt-circle-left"></i></span>
				<h6 class="tituloPagina"><?php echo get_the_title() ?></h6>
				<div class="conteudo">
					
				<?php
		
				 echo the_content();

				 ?>
				</div>
				<a href="<?php echo get_home_url()."/contato"; ?>" target="_blank" class="linkModal">Contato</a>
			</section>
		</div>
		<div class="col-sm-6">
			<div class="imagensLink">
				<ul>
					<?php 
						$galeria_imagens_modelo = rwmb_meta('Plataforma_galeria_modelo');
						foreach ($galeria_imagens_modelo as $galeria_imagens_modelo ):
							$galeria_imagens_modelo = $galeria_imagens_modelo;
					?>
					<li class="imagensGaleria">
						<img src="<?php echo $galeria_imagens_modelo["full_url"] ?>">
					</li>
				<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();
