<?php

/**
 * Plugin Name: Base Amanda Karoline Plataforma
 * Description: Controle base do tema Amanda Karoline Plataforma
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: https://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseProjeto () {

		// TIPOS DE CONTEÚDO
		conteudosProjeto();

		taxonomiaProjeto();

		metaboxesProjeto();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosProjeto (){

		// TIPOS DE CONTEÚDO
		tipoModelos();
		// TIPOS DE CONTEÚDO
		tipoParceiros();

		// TIPOS DE CONTEÚDO
		tipoPlataformas();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'modelos':
					$titulo = 'Nome da modelo';
				break;

				case 'parceiros':
					$titulo = 'Nome do parceiro';
				break;

				case 'plataformas':
					$titulo = 'Nome da plataforma';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE MODELOS PARTICIPANTES
	function tipoModelos() {

		$rotulosModelos = array(
			'name'               => 'Modelos Participantes',
			'singular_name'      => 'Modelos Participantes',
			'menu_name'          => 'Modelos Participantes',
			'name_admin_bar'     => 'Modelos participantes',
			'add_new'            => 'Adicionar nova modelo participante',
			'add_new_item'       => 'Adicionar nova modelo participante',
			'new_item'           => 'Nova modelo participante',
			'edit_item'          => 'Editar modelos participantes',
			'view_item'          => 'Ver modelos participantes',
			'all_items'          => 'Todas as modelos participantes',
			'search_items'       => 'Buscar modelos participantes',
			'parent_item_colon'  => 'Das modelos participantes',
			'not_found'          => 'Nenhuma modelo cadastrada.',
			'not_found_in_trash' => 'Nenhuma modelo na lixeira.'
		);

		$argsModelos 	= array(
			'labels'             => $rotulosModelos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-heart',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'modelos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('modelos', $argsModelos);

	}

	function tipoParceiros() {

		$rotulosParceiros = array(
			'name'               => 'Parceiros',
			'singular_name'      => 'Parceiro',
			'menu_name'          => 'Parceiros',
			'name_admin_bar'     => 'Parceiros',
			'add_new'            => 'Adicionar novo parceiro',
			'add_new_item'       => 'Adicionar nova parceiro',
			'new_item'           => 'Novo parceiro',
			'edit_item'          => 'Editar parceiros',
			'view_item'          => 'Ver parceiros',
			'all_items'          => 'Todas os parceiros',
			'search_items'       => 'Buscar parceiros',
			'parent_item_colon'  => 'Dos parceiros',
			'not_found'          => 'Nenhum parceiro cadastrada.',
			'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
		);

		$argsParceiros 	= array(
			'labels'             => $rotulosParceiros,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-groups',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'parceiros' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceiros', $argsParceiros);

	}

	function tipoPlataformas() {

		$rotulosPlataformas = array(
			'name'               => 'Plataformas',
			'singular_name'      => 'Plataforma',
			'menu_name'          => 'Plataformas',
			'name_admin_bar'     => 'Plataformas',
			'add_new'            => 'Adicionar nova plataforma',
			'add_new_item'       => 'Adicionar nova plataforma',
			'new_item'           => 'Nova plataforma',
			'edit_item'          => 'Editar plataformas',
			'view_item'          => 'Ver plataformas',
			'all_items'          => 'Todas as plataformas',
			'search_items'       => 'Buscar plataformas',
			'parent_item_colon'  => 'Das plataformas',
			'not_found'          => 'Nenhuma plataforma cadastrada.',
			'not_found_in_trash' => 'Nenhuma plataforma na lixeira.'
		);

		$argsPlataformas 	= array(
			'labels'             => $rotulosPlataformas,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-layout',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'plataformas' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('plataformas', $argsPlataformas);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomiaProjeto () {		
			taxonomiaCategoriaModelos();

			taxonomiaPlataformas();

		}
		function taxonomiaCategoriaModelos() {

			$rotulosCategoriaModelos = array(
				'name'              => 'Categorias',
				'singular_name'     => 'Categoria',
				'search_items'      => 'Buscar categorias',
				'all_items'         => 'Todas as categorias',
				'parent_item'       => 'Categoria pai',
				'parent_item_colon' => 'Categoria pai:',
				'edit_item'         => 'Editar categoria',
				'update_item'       => 'Atualizar categoria',
				'add_new_item'      => 'Nova categoria',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias',
				);

			$argsCategoriaModelos 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaModelos,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-modelos' ),
				);

			register_taxonomy( 'categoriaModelos', array( 'modelos' ), $argsCategoriaModelos );

		}	

		function taxonomiaPlataformas() {

			$rotulosCategoriaPlataformas = array(
				'name'              => 'Categorias',
				'singular_name'     => 'Categoria',
				'search_items'      => 'Buscar categorias',
				'all_items'         => 'Todas as categorias',
				'parent_item'       => 'Categoria pai',
				'parent_item_colon' => 'Categoria pai:',
				'edit_item'         => 'Editar categoria',
				'update_item'       => 'Atualizar categoria',
				'add_new_item'      => 'Nova categoria',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias',
				);

			$argsCategoriaPlataformas =  array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaPlataformas,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-plataformas' ),
				);

			register_taxonomy( 'categoriaPlataformas', array( 'plataformas' ), $argsCategoriaPlataformas );

		}		

    /****************************************************
	* META BOXES
	*****************************************************/
		function metaboxesProjeto(){

			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

		}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Plataforma_';

			// METABOX
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxParceiros',
				'title'			=> 'Detalhes do parceiro',
				'pages' 		=> array( 'parceiros' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Link do parceiro: ',
						'id'    => "{$prefix}link_parceiro",
						'desc'  => 'Insira aqui o link do parceiro',
						'type'  => 'text'
					),
				),
			);

			// METABOX
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxPlataformas',
				'title'			=> 'Detalhes da plataforma',
				'pages' 		=> array( 'plataformas' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Link da plataforma: ',
						'id'    => "{$prefix}link_plataforma",
						'desc'  => 'Insira aqui o link da plataforma',
						'type'  => 'text'
					),
					
					array(
						'name'  => 'Galeria de imagens da plataforma:',
						'id'    => "{$prefix}galeria_plataforma",
						'desc'  => 'Galeria de imagens da plataforma',
						'type'  => 'image_advanced',
					), //  A IMAGEM DESTAQUE VAI FICAR COMO FUNDO DA GALERIA DO SERVIÇO
					// O EDITOR DO WORDPRESS SERVE NESTE CASO PARA DEFINIR A DESCRIÇÃO DO SERVICO 

				),
			);

			// METABOX
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxModelos',
				'title'			=> 'Detalhes da Modelo',
				'pages' 		=> array( 'modelos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Link da modelo: ',
						'id'    => "{$prefix}link_contatoPlataforma",
						'desc'  => 'Insira aqui o link de contato',
						'type'  => 'text'
					),
					
					array(
						'name'  => 'Galeria de imagens da Modelo:',
						'id'    => "{$prefix}galeria_modelo",
						'desc'  => 'Galeria de imagens da Modelo',
						'type'  => 'image_advanced',
					),
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
		function shortcodesProjeto(){

		}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
		function visualcomposerProjeto(){

		    if (class_exists('WPBakeryVisualComposer')){

			}

		}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseProjeto');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseProjeto();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );